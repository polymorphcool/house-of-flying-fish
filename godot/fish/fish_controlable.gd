extends Spatial

const FRONT = Vector3(0,0,-1)
const UP = Vector3(0,1,0)

export (bool) var synch_materials = false setget do_synch_materials
export (bool) var eye_tracking = true setget set_eye_tracking
export (bool) var cam_tracking = true setget set_cam_tracking
export (float,-5,5) var curve_x = 0 setget set_curve_x
export (float,-5,5) var curve_y = 0 setget set_curve_y
export (float,0,5) var multiply = 1 setget set_multiply
export (float,0,10) var speed = 1
export (float,0,5) var mesh_reactivity = 0.9
export (float,0,5) var pivot_reactivity = 0.5
export(float,-1.8,1.8) var _yaw = 0 setget yaw
export(float,-1.8,1.8) var _pitch = 0 setget pitch
export(float,0,1) var pitch_sustain = 0.8

onready var direction = FRONT
onready var lookat_pos = $cam_track.translation
var mat_body = null
var mat_eye = null
var timer = 0

var current_dir = Basis()
var target_dir = Basis()
var cam_up = UP

func look( obj ):
		
	if obj == null:
		set_eye_tracking(false)
		
	if not obj is Spatial:
		print( "i don't want to look at that" )
		return
	
	eye_tracking = true
	obj.global_transform.origin = $mesh/gaze.global_transform.origin

func do_synch_materials( b ):
	if b:
		init()

func set_eye_tracking( b ):
	eye_tracking = b
	if not b:
		mat_eye.set_shader_param( "uv1_offset", Vector3(0,0,0) )

func set_cam_tracking( b ):
	cam_tracking = b
	if not b:
		$pivot/cam.rotation = Vector3(0,PI,0)

func set_curve_x( f ):
	curve_x = f
	update_curvature()

func set_curve_y( f ):
	curve_y = f
	update_curvature()

func update_curvature():
	if mat_body == null:
		return
	if abs(curve_x) < 1e-5:
		curve_x = 0
	if abs(curve_y) < 1e-5:
		curve_y = 0
	var curv = Vector3( curve_x, curve_y, 0.0 )
	mat_body.set_shader_param( "curvature", curv )
	mat_eye.set_shader_param( "curvature", curv )

func set_multiply( f ):
	multiply = f
	mat_body.set_shader_param( "factor_mult", multiply )
	mat_eye.set_shader_param( "factor_mult", multiply )

func yaw( rad = null ):
	if rad == null:
		return _yaw
	_yaw = rad
	update_target()

func pitch( rad = null ):
	if rad == null:
		return _pitch
	_pitch = rad
	update_target()

func update_target():
	var qx = Quat( Vector3(1,0,0), _pitch )
	var qy = Quat( Vector3(0,1,0), _yaw  )
	target_dir = Basis( qy * qx )

func init():
	mat_body = $mesh.mesh.surface_get_material(0)
	mat_eye = $mesh.mesh.surface_get_material(1)
	# setting vars
	update_curvature()
	mat_body.set_shader_param( "factor_mult", multiply )
	mat_eye.set_shader_param( "uv1_offset", Vector3(0,0,0) )
	# synching mats
	mat_eye.set_shader_param( "curvature", mat_body.get_shader_param( "curvature" ) )
	mat_eye.set_shader_param( "curve_size", mat_body.get_shader_param( "curve_size" ) )
	mat_eye.set_shader_param( "curve_offset", mat_body.get_shader_param( "curve_offset" ) )
	mat_eye.set_shader_param( "curve_accel", mat_body.get_shader_param( "curve_accel" ) )
	mat_eye.set_shader_param( "timer", mat_body.get_shader_param( "timer" ) )
	mat_eye.set_shader_param( "swim_mult", mat_body.get_shader_param( "swim_mult" ) )
	mat_eye.set_shader_param( "factor_mult", mat_body.get_shader_param( "factor_mult" ) )
	mat_eye.set_shader_param( "twist_factor", mat_body.get_shader_param( "twist_factor" ) )
	mat_eye.set_shader_param( "s_factor", mat_body.get_shader_param( "s_factor" ) )
	mat_eye.set_shader_param( "yaw_factor", mat_body.get_shader_param( "yaw_factor" ) )
	mat_eye.set_shader_param( "head_z", mat_body.get_shader_param( "head_z" ) )
	mat_eye.set_shader_param( "head_color", mat_body.get_shader_param( "head_color" ) )
	mat_eye.set_shader_param( "tail_z", mat_body.get_shader_param( "tail_z" ) )
	mat_eye.set_shader_param( "tail_color", mat_body.get_shader_param( "tail_color" ) )

func _ready():
	init()
	pass

func _process(delta):

	$current_dir.rotation = current_dir.get_euler()
	$target_dir.rotation = target_dir.get_euler()
	
	var diff = (target_dir * current_dir.inverse()).get_euler()
	var mesh_base = target_dir.slerp( current_dir, delta )
	current_dir = current_dir.slerp( target_dir, delta )
	
	$pivot.rotation = current_dir.get_euler()
	$mesh.rotation = mesh_base.get_euler()
	$mesh.rotation.z = diff.y * -0.2
	
	direction = ($mesh.global_transform.origin - $mesh.to_global( FRONT )).normalized()

	var timer_speed = speed
	timer_speed = 1 + max(abs( diff.y ),abs( diff.x ))
	timer += timer_speed * delta
	mat_body.set_shader_param( "timer", timer )
	mat_eye.set_shader_param( "timer", timer )

	if eye_tracking:
		var xz = Vector2( $mesh/gaze.translation.x, $mesh/gaze.translation.z ).normalized()
		var rotx = max( -0.5, min( 0.5, ( 0.5 - ( xz.angle() / PI ) ) * 0.5) )
		var xy = Vector2( $mesh/gaze.translation.y, $mesh/gaze.translation.z ).normalized()
		var roty = max( -0.5, min( 0.5, ( 0.5 - ( xy.angle() / PI ) ) * 0.5) )
		var uv1off = Vector3( rotx, roty, 0 )
		mat_eye.set_shader_param( "uv1_offset", uv1off )

	if cam_tracking:
		var ctt = lookat_pos + Vector3( 0, -diff.x, 0 )
		$cam_track.translation += ( ctt - $cam_track.translation ) * 5 * delta
		$pivot/cam.look_at( to_global( $cam_track.translation ), cam_up )
	
	set_curve_x( diff.y * 5 / PI )
	set_curve_y( -diff.x * 5 / PI )
	pitch( _pitch - _pitch * pitch_sustain * delta )
	
	
