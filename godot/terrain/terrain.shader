shader_type spatial;
render_mode blend_mix,depth_draw_alpha_prepass,cull_back,diffuse_burley,specular_schlick_ggx;

uniform sampler2D cliff_albedo : hint_albedo;
uniform sampler2D cliff_roughness : hint_white;
uniform sampler2D cliff_normal : hint_normal;

uniform sampler2D top_albedo : hint_albedo;
uniform sampler2D top_roughness : hint_white;
uniform sampler2D top_normal : hint_normal;

uniform sampler2D bottom_albedo : hint_albedo;
uniform sampler2D bottom_roughness : hint_white;
uniform sampler2D bottom_normal : hint_normal;

uniform vec4 cliff_color : hint_color;
uniform vec4 top_color : hint_color;
uniform vec4 bottom_color : hint_color;

uniform float specular;
uniform float metallic;
uniform float distance_fade_min;
uniform float distance_fade_max;
uniform float roughness : hint_range(0,1);
uniform float point_size : hint_range(0,128);
uniform vec4 roughness_texture_channel;
uniform float normal_scale : hint_range(0,16);
uniform vec3 uv1_scale;
uniform vec3 uv1_offset;
uniform vec3 uv2_scale;
uniform vec3 uv2_offset;

uniform sampler2D height_map : hint_albedo;
uniform sampler2D height_normal : hint_normal;

uniform float height_mult = 1.0;
uniform vec2 height_scale = vec2( 1.0, 1.0 );
uniform vec2 height_offset = vec2( 0.0, 0.0 );

uniform float height_min = 0.0;
uniform float height_max = 1.0;

uniform float mixer_min = 0.4;
uniform float mixer_max = 0.8;

uniform float rotation = 0.0;

varying smooth vec2 base_uv;
varying smooth vec2 heigh_uv;
varying smooth vec2 mix_uv;
varying smooth float height;
varying smooth float slope;

float get_depth( vec2 uv ) {
	return texture( height_map, uv ).r * height_mult;
}

vec2 rotated_v2( vec2 v, float a ) {
	float c = cos( a );
	float s = sin( a );
	return vec2( c*v.x - s*v.y, s*v.x + c*v.y );
}

void vertex() {
	// height map uvs are generated based on the absolute position of the VERTEX
	vec3 hv = (WORLD_MATRIX * vec4(VERTEX, 1.0)).xyz;
	heigh_uv = hv.xz * height_scale + height_offset;
	mix_uv = heigh_uv;
	base_uv = heigh_uv*uv1_scale.xy + uv1_offset.xy;
	float h = get_depth(heigh_uv);
	VERTEX.y = h;
	height = h / height_mult;
	height = ( max( height_min, min( height_max, height ) ) - height_min ) / ( height_max - height_min );
	vec3 hn = texture(height_normal, heigh_uv).xyz;
	hn = mat3(WORLD_MATRIX[0].xzy,WORLD_MATRIX[1].xzy,WORLD_MATRIX[2].xzy) * hn.xyz;
//	hn = ( hn * 2.0 ) - vec3(1.0,1.0,1.0);
//	slope = dot( hn, vec3(0.0,0.0,1.0) );
	slope = hn.y;
	slope = max( 0.0, min( 1.0, mixer_min + ( ( hn.y - mixer_min ) / mixer_max ) ) );
}

void fragment() {
	
	float mixer = slope;
//	mixer = ( max( mixer_min, min( mixer_max, mixer ) ) - mixer_min ) / ( mixer_max - mixer_min );
	
	// computing textures
	vec4 abldedo_c = mix( texture(bottom_albedo,base_uv), texture(top_albedo,base_uv), height );
	abldedo_c = mix( texture(cliff_albedo,base_uv), abldedo_c, mixer );
	
	vec4 albedo_color = mix(  bottom_color, top_color, height );
	albedo_color = mix( cliff_color, albedo_color, mixer );
	
	float rough_b = dot(texture(bottom_roughness,base_uv),roughness_texture_channel);
	float rough_t = dot(texture(top_roughness,base_uv),roughness_texture_channel);
	float rough_c = dot(texture(cliff_roughness,base_uv),roughness_texture_channel);
	float rough = mix( rough_c, mix( rough_b, rough_t, height ), mixer);
	
	vec3 norm = mix( texture(bottom_normal,base_uv), texture(top_normal,base_uv), height ).rgb;
	norm = mix( texture(cliff_normal,base_uv).rgb, norm, mixer );
	
	// applying
	ALBEDO = abldedo_c.rgb * albedo_color.rgb;
	METALLIC = metallic;
	ROUGHNESS = rough * roughness;
	SPECULAR = specular;
	
	vec3 hnorm = texture(height_normal,heigh_uv).rgb;
	int stepx = int( floor( heigh_uv.x ) );
	int stepy = int( floor( heigh_uv.y ) );
	if ( stepx % 2 == 1 ) {
		hnorm.x = 1.0-hnorm.x;
	} 
	if ( stepy % 2 == 1 ) {
		hnorm.y = 1.0-hnorm.y;
	}
//	hnorm = vec3( rotated_v2( hnorm.xy, -rotation ), hnorm.z );
	vec3 normmap = mix( hnorm, norm, normal_scale / height_mult );
	NORMALMAP = hnorm;
	NORMALMAP_DEPTH = normal_scale; // height_mult;
	
	vec2 big_uv = UV * vec2(100.0);
	if ( big_uv.y <= 0.5 ) {
		ALPHA = big_uv.y*2.0;
	} else {
		ALPHA = 1.0;
	}
	
//	ALPHA = albedo.a * albedo_tex.a;
	ALPHA*=clamp(smoothstep(distance_fade_min,distance_fade_max,-VERTEX.z),0.0,1.0);
}