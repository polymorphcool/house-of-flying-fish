extends Spatial

export (float,0,100) var speed : float = 10
export (float,-10,10) var speed_rot : float = 1

var a = 0

func _ready():
	pass

func _process(delta):
	
	a += delta * speed_rot
	var dir = Vector3( 0,0, delta * -speed ).rotated( Vector3(0,1,0), a )
	$ground_ctrl.rotation.y = a
	$ground_ctrl.translation += dir
