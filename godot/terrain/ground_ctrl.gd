tool

extends Position3D

export (float,-100,100) var _height : float = 15 setget height
export (float,0,0.05) var _height_scale : float = 0.001 setget height_scale

var min_step = null
var previous_transform = null
var gmat = null
var h_scale : Vector2 = Vector2(0,0)
var h_offset : Vector2 = Vector2(0,0)
var im_repeat : int = 0
var im : Image = null

func height( f ):
	_height = f
	if gmat != null:
		$offset/ground.material_override.set_shader_param( "height_mult", _height )

func height_scale( f ):
	_height_scale = f
	h_scale = Vector2( _height_scale, _height_scale )
	if gmat != null:
		$offset/ground.material_override.set_shader_param( "height_scale", h_scale )

func initialise():
	
	min_step = $offset/ground_back.mesh.size.x / ( $offset/ground_back.mesh.subdivide_width + 1 )
	print( min_step )
	gmat = $offset/ground.material_override
	h_offset = $offset/ground.material_override.get_shader_param( "height_offset" )
	
	var tex : Texture = $offset/ground.material_override.get_shader_param( "height_map" )
	im = tex.get_data()
	if im.is_compressed():
# warning-ignore:return_value_discarded
		im.decompress()
	im.lock()
	im_repeat = tex.flags
	if im_repeat & Texture.FLAG_MIRRORED_REPEAT != 0:
		im_repeat = Texture.FLAG_MIRRORED_REPEAT
		print( "FLAG_MIRRORED_REPEAT" )
	elif im_repeat & Texture.FLAG_REPEAT != 0:
		im_repeat = Texture.FLAG_REPEAT
		print( "FLAG_REPEAT" )
	else:
		im_repeat = 0
	
	height( _height )
	height_scale( _height_scale )

func _ready():
	initialise()

func bound( v2, repeat ):
	
	var out = { 'pos': v2, 'symmetry': null }
	
	# already good! no need to go further
	if v2.x >= 0 and v2.x <= 1 and v2.y >= 0 and v2.y <= 1:
		return out
	
	# constraining x & y in range [-1,2]
	while out.pos.x < -1:
		out.pos.x +=1
	while out.pos.x > 2:
		out.pos.x -=1
	while out.pos.y < -1:
		out.pos.y +=1
	while out.pos.y > 2:
		out.pos.y -=1
	
	# now the real stuff
	match repeat:
		# simple, just sending the position to the other side
		Texture.FLAG_REPEAT:
			# simple repeat
			if out.pos.x < 0:
				out.pos.x +=1
			elif out.pos.x > 1:
				out.pos.x -=1
			if out.pos.y < 0:
				out.pos.y +=1
			elif out.pos.y > 1:
				out.pos.y -=1
		# mirror, lot more complex: requires a symmetry to be applied on direction
		Texture.FLAG_MIRRORED_REPEAT:
			if out.pos.x < 0:
				out.pos.x = -out.pos.x
				out.symmetry = Vector2(-1,1)
			elif out.pos.x > 1:
				out.pos.x = 1 - (out.pos.x-1)
				out.symmetry = Vector2(-1,1)
			if out.pos.y < 0:
				out.pos.y = -out.pos.y
				if out.symmetry == null:
					out.symmetry = Vector2(1,1)
				out.symmetry.y = -1
			elif out.pos.y > 1:
				out.pos.y = 1 - (out.pos.y-1)
				if out.symmetry == null:
					out.symmetry = Vector2(1,1)
				out.symmetry.y = -1
		# just block the value at border, no teleportation, no symmetry
		_:
			if out.pos.x < 0:
				out.pos.x = 0
			if out.pos.x > 1:
				out.pos.x = 1
			if out.pos.y < 0:
				out.pos.y = 0
			if out.pos.y > 1:
				out.pos.y = 1
	return out

func get_color( pos : Vector2 ):
	
	# position MUST be bounded!
	var map_size = im.get_size()
	var pixid = ( map_size - Vector2(1,1) ) * pos
	
	if pixid.x > map_size.x or pixid.y > map_size.y:
		print( "!!!!! ", pos )
		return Vector3(1,0,0)
	
	var weight = Vector2( pixid.x - floor(pixid.x), pixid.y - floor(pixid.y) )
	var kernel = [
		[floor(pixid.x), 1 - weight.x],
		[floor(pixid.y), 1 - weight.y],
		[int(ceil(pixid.x)) % int(map_size.x), weight.x],
		[int(ceil(pixid.y)) % int(map_size.y), weight.y]
	]
	var colors = [
		[im.get_pixel( kernel[0][0], kernel[1][0] ), kernel[0][1]+kernel[1][1]],
		[im.get_pixel( kernel[0][0], kernel[3][0] ), kernel[0][1]+kernel[3][1]],
		[im.get_pixel( kernel[2][0], kernel[1][0] ), kernel[2][1]+kernel[1][1]],
		[im.get_pixel( kernel[2][0], kernel[3][0] ), kernel[2][1]+kernel[3][1]]
	]
	var mixed = Color(0,0,0)
	var totalw = 0
	for mx in colors:
		mixed += mx[0] * mx[1]
		totalw += mx[1]
	mixed /= totalw
	return mixed

# warning-ignore:unused_argument
func _process(delta):
	
	if gmat == null:
		initialise()
	if gmat == null:
		return
	
	if previous_transform != transform:
		
		previous_transform = transform
		
		var gt = translation
		while gt.x > min_step:
			gt.x -= min_step
		while gt.x < -min_step:
			gt.x += min_step
		while gt.z > min_step:
			gt.z -= min_step
		while gt.z < -min_step:
			gt.z += min_step
		gt *= Vector3( -1,0,-1 )
		$offset.translation = gt
		$offset/ground.material_override.set_shader_param( "rotation", rotation.y )
	
		# position of cam pivot
		var rel = $cam_pivot.global_transform.origin
		var map_size = im.get_size()
		var rel2d = Vector2( rel.x, rel.z ) * h_scale + h_offset
		var bounded = bound( rel2d, im_repeat )
		rel2d = bounded.pos
		var c = im.get_pixelv( rel2d * map_size )
#		$cam_pivot.translation.y = c.r * _height
		get_node( "../debug/cam" ).position = rel2d * map_size * 0.5
